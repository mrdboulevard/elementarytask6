import java.util.ArrayList;

public class Lottery {

    private int startNumber, endNumber;

    Lottery(int startNumber, int endNumber) {
        this.startNumber = startNumber;
        this.endNumber = endNumber;
    }

    void calculate() {
        // 0, 1
        // 000000, 000001
        ArrayList<String> tickets = generateTickets();
        int countHappyTicketsEasy = easyMethod(tickets);
        int countHappyTicketsHard = hardMethod(tickets);

        System.out.println("Количество счасливых билетов в простом методе: " + countHappyTicketsEasy);
        System.out.println("Количество счасливых билетов в сложном методе: " + countHappyTicketsHard);

        if (countHappyTicketsEasy > countHappyTicketsHard) {
            System.out.println("Простой метод собрал больше счастливых билетов!");
        } else if (countHappyTicketsEasy < countHappyTicketsHard) {
            System.out.println("Сложный метод собрал больше счастливых билетов!");
        } else {
            System.out.println("Методы собрали поровну счасливых билетов!");
        }
    }

    private ArrayList<String> generateTickets() {
        ArrayList<String> listTickets = new ArrayList<String>();

        // 99999 - 5 символів
        // 100000 - 6 символів
        for (int i = startNumber; i <= endNumber; i++) {
            //if (String.valueOf(i).length() < 6) {
            if (i < 100000) {
                // i = 12
                // numberString = "12" // length = 2
                String numberString = String.valueOf(i);
                // for complete number
                String completeNumber = "";

                // 6 - numberString.length() // 6 - 2 // 4
                for (int j = 0; j < 6 - numberString.length(); j++) {
                    completeNumber += "0";
                }

                // 0000 12
                completeNumber += numberString;

                listTickets.add(completeNumber);
            } else {
                // 100000 -> "100000"
                String completeNumber = String.valueOf(i);
                listTickets.add(completeNumber);
            }
        }

        return listTickets;
    }

    private int easyMethod(ArrayList<String> tickets) {
        // Кількість щасливих білетів
        int countTicket = 0;

        for (String ticket : tickets) {

            String[] numbers = ticket.split("");

            int firstHalfSum = 0;
            int secondHalfSum = 0;

            // 0 1 2 3 4 5
            // 1 2 3 4 5 6 - число 123456

            // 1 + 2 + 3 = 6
            for (int j = 0; j < numbers.length / 2; j++) {
                firstHalfSum += Integer.parseInt(numbers[j]);
            }

            // 4 + 5 + 6 = 15
            for (int j = numbers.length / 2; j < numbers.length; j++) {
                secondHalfSum += Integer.parseInt(numbers[j]);
            }

            if (firstHalfSum == secondHalfSum) {
                countTicket++;
            }

        }

        return countTicket;
    }

    public int hardMethod(ArrayList<String> tickets) {
        int countTicket = 0;

        for (String ticket : tickets) {
            String[] numbers = ticket.split("");

            int firstHalfSum = 0;
            int secondHalfSum = 0;

            // 0 1 2 3 4 5
            // 1 2 3 4 5 6 - число 123456

            // 0 + 2 + 4 = 6
            for (int j = 0; j < numbers.length; j += 2) {
                firstHalfSum += Integer.parseInt(numbers[j]);
            }

            // 1 + 3 + 5 = 9
            for (int j = 1; j < numbers.length; j += 2) {
                secondHalfSum += Integer.parseInt(numbers[j]);
            }

            if (firstHalfSum == secondHalfSum) {
                countTicket++;
            }

        }
        return countTicket;
    }

}
