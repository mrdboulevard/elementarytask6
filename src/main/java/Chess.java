public class Chess {

    private int height, width;

    Chess(int height, int width) {
        this.height = height;
        this.width = width;
    }


    public void build() {

        // висота 4
        // ширина 4

        // Ідем по висоті
        for (int i = 0; i < height; i++) {

            // 0
            // Ідем по широні
            // 1
            for (int j = 0; j < width; j++) {

                // i = 0 1 2 3 4
                // i % 2
                //     0 5 0 5 0

                if (i % 2 != 0) {

                    // j = 0 1 2 3 4
                    // j % 2
                    //     0 5 0 5 0
                    if (j % 2 == 0) {
                        System.out.print(" ");
                    } else {
                        System.out.print("*");
                    }

                } else {

                    // j = 0 1 2 3 4
                    // j % 2
                    //     0 5 0 5 0
                    if (j % 2 != 0) {
                        System.out.print(" ");
                    } else {
                        System.out.print("*");
                    }

                }
            }

            System.out.println();
        }
    }
}
